# -*- coding: utf-8 -*-
import unittest
import json
import os
import sys

module_src = os.path.abspath(os.getcwd() + '\\..')
sys.path.append(module_src)
print(module_src)

module_path = os.path.abspath(os.getcwd() + '\\..\\functions')
sys.path.append(module_path)

print (sys.path)


from src.functions.Functions import Functions as Selenium
from src.functions.Inicializar import Inicializar as Inicializar
from functools import reduce
from datetime import datetime
import allure



@allure.feature(u'Proyecto de Automatización')
@allure.story(u'001: Front - BDD - API')
@allure.testcase(u"Caso de Prueba 001")
@allure.severity(allure.severity_level.NORMAL)
@allure.description(u"""Se hace una prueba integral de Automatización:</br>
Se extraerá de la Aplicación Web, de la Base de Datos y de API un dato y se comprobara si estos datos coinciden </br>
 </br></br>""")
class Prueba_APIS(Selenium, unittest.TestCase):

    def setUp(self):
        # Datos para conexión a Base de Datos
        self.host = "127.0.0.1"
        self.port = "3306"
        self.user = "root"
        self.pasw = ""
        self.database = "api-rest"

        # Datos para API
        self.endpoint = "http://localhost/Api_rest/controller/getAll.php"
        self.tipoPeticion = "GET" # GET, POST, PUT, DELETE

        # Datos del caso
        self.id_producto = "2"
        Inicializar.cantidad_reintentos = 10
        Inicializar.tiempo_entre_reintentos = 1

    def test_000_obtener_productos(self):
        with allure.step(u'Paso 1: Interactuar con la App Web'):
            Selenium.get_json_file(self, "Api_rest")
            Selenium.abrir_navegador(self, "http://127.0.0.1/Api_Rest/view/")
            Selenium.page_has_loaded(self)
            Selenium.send_key_text(self, "TXT_Cuadro", '2')
            Selenium.get_elements(self, "BTN_Consulta").click()
            Selenium.highlight(self, "TXT_Cuadro")
            Selenium.esperar(3)


        # Se obtiene el nombre del producto desde la APP Web.
        with allure.step(u'Paso 2: Se Obtiene Informacion de la App Web'):
            nombre_producto = Selenium.get_elements(self, "<tr>_Producto").text

        # Se obtiene el nombre del producto desde la Base de Datos.
        with allure.step(u'Paso 3: Se obtiene información de Base de Datos'):
            query = f"SELECT name FROM products WHERE id = {self.id_producto}"
            results = Selenium.get_list_base_mysql(self, self.host, port=self.port, user=self.user, password=self.pasw,
                                               database=self.database, query=query)
            nombre_producto_db = results[0][0]
            print( "La consulta a la BDD arrojo el siguiente Resultado: " + str(nombre_producto_db))
            print("\n")

        # Se obtiene el nombre del producto desde la API.
        with allure.step(u'Paso 4: Se conecta y se obtiene informacion de la API'):
            response = Selenium.send_Service(self.tipoPeticion, self.endpoint)
            if response.status_code == 200:
                print("El estado del servicio es: " + str(response.status_code))
                print("\n")
                print("Los datos obtenidos desde la API son: " + response.text)

                print("\n")
                for response in response.json().items():
                    dict_productos = response[1]
                    for i in range(len(dict_productos)):
                        # Converción de diccionario(json) a lista(array)
                        arr_prdocutos = list(reduce(lambda x, y: x + y, dict_productos[i].items()))

                        #print(arr_prdocutos)

                        # Buscamos el 'ID del Producto' (self.id_producto) en el array actual.
                        try:
                            if arr_prdocutos.index(self.id_producto):
                                self.nombre_producto_api = arr_prdocutos[3]
                                #print(nombre_producto_api)
                        except:
                            pass
                            #print("No se encuentra el ID")

        # Se compara el Nombre del Producto obtenido en la APP Web con el de la Base de Datos.
        with allure.step(u'Paso 5: Se Compara el Dato de App con la BdD'):
            if nombre_producto == nombre_producto_db:
                print ("La comparacion de datos Obtenidos del Front y de la Base de Datos resulto Exitosa")
            else:
                print ("El nombre del producto es incorrecto.")

        print("\n")

        # Se compara el Nombre del Producto obtenido en la Base de Datos con el de la API.
        with allure.step(u'Paso 7:Se compara el dato de la BdD con la API'):
            if nombre_producto_db == self.nombre_producto_api:
                print ("La Comparacion de datos obtenidos de la Base de Datos y Api resulto Exitosa")
            else:
                print ("El nombre del producto es distinto.")

        print("\n")
        # Se compara el Nombre del Producto obtenido en la APP Web con el de la API.
        with allure.step(u'Paso 7:Se comprar el Dato de la App web con la API'):
            if nombre_producto == self.nombre_producto_api:
                print ("La Comparacion de datos obtenidos del Front y Api resulto Exitosa")
            else:
                print ("El nombre del producto es distinto.")

    def test_001_crear_producto_api(self):
        with allure.step(u'Paso 1: Se Estable Conexion con la API y se Carga Query '):
            tipo_peticion = "POST"
            endpoint = "http://localhost/api/product/create.php"

            now = datetime.now()
            dt_string = now.strftime("%Y/%m/%d %H:%M:%S")

            headers = {'Content-Type': 'application/json'}
            payload = json.dumps({
                "name": "Agua",
                "price": "300",
                "description": "Mineralizada",
                "category_id": 3,
                "created": str(dt_string)
            })

        with allure.step(u'Paso 2:Se captura el resultado de la conexión con la API'):
            response = Selenium.send_Service(tipo_peticion, endpoint, headers, payload)
            if response.status_code == 201:
                print("Datos insertados correctamente")
            else:
                print("Error, el estado del servicio es: " + str(response.status_code))


    def tearDown(self):
        with allure.step(u'Paso 3:Se cierra el Caso'):
            pass

if __name__ == '__main__':
    unittest.main()